<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type','phone', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setImage($filePath)
    {
        $filePath = $filePath->getRealPath();
        $file = file_get_contents($filePath);

        $fileName = 'user/' . $this->id . "/image.jpg";

        if(!empty($this->attributes['image']) && Storage::exists($this->attributes['image']))
            Storage::disk('public')
                ->delete($this->attributes['image']);

        Storage::disk('public')
            ->put($fileName, $file);

        $this->image = $fileName;
        $this->save();
    }

    public function getImageAttribute()
    {
        if($this->attributes['image'] && !str_contains($this->attributes['image'], 'http'))
            return Storage::disk('public')
                ->url($this->attributes['image']);

        return $this->attributes['image'];
    }

}
